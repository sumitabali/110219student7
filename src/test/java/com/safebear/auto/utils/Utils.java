package com.safebear.auto.utils;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Utils {


    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");

    private static final String BROWSER = System.getProperty("browser", "chrome");


    //adding the 2 variables
    public static String getUrl() {
        return URL;

    }

    public static WebDriver getDriver() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/Drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case ("chrome"):
            case "headless":
                options.addArguments("headless","disable-gpu");
                return new ChromeDriver(options);

            default:
                return new ChromeDriver(options);
        }


    }
}






