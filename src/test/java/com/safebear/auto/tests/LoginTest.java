package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolPage;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class LoginTest {

    //getiing our driver e.g chromwdriver
    WebDriver driver = Utils.getDriver();

    //Connecting to our pages (creating objects of our pages)
    LoginPage loginPage = new LoginPage(driver);
    ToolPage ToolPage = new ToolPage(driver);

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    //Test for valid login
    @Test
    public void testValidLogin() {

        //Go to login page
        driver.get(Utils.getUrl());
        //Enter login details
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");


        //Click on login
        loginPage.clickLoginButton();

        //Assert message is correct
        Assert.assertTrue(ToolPage.successfullLoginMessage().contains("Login Successfull"));
    }

    //Test for invalid login
    @Test
    public void testInvalidLogin() {

        driver.get(Utils.getUrl());
        //Enter login details
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");


        //Click on login
        loginPage.clickLoginButton();
        //Assert message is correct
        Assert.assertTrue(ToolPage.successfullLoginMessage().contains("Login Successfull"));
    }
}
