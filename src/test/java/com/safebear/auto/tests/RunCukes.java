package com.safebear.auto.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(

        //This is where my html report wil be (in the target/cucumber directory)
        plugin = {"pretty","html:target/cucumber"},

        //Tis is changing to 'not @to-do' in future cucumber releases
        tags="~@to-do",

        glue = "com.safebear.auto.tests",

        features = "classpath:toolList.features/Login.feature"
)

        public class RunCukes extends AbstractTestNGCucumberTests {


}

