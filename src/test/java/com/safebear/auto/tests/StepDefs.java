package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class StepDefs {


   //defining the driver
    WebDriver driver;
    LoginPage loginPage;
    ToolPage toolPage;


    //method for driver before running the test
    @Before
    public void setUp(){
        driver= Utils.getDriver();
        loginPage =new LoginPage(driver);
        toolPage =new ToolPage(driver);
    }

    //method after running the  to close the browser
    @After
    public void tearDown(){

        //This pause for a couple of seconds after the test has run
            try {
                Thread.sleep(Integer.parseInt(System.getProperty("sleep","2000")));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            driver.quit();
        }

        @Given("I navigate to the login page")
    public void  i_navigate_to_the_login_page() {
        // Write code here that turns the phrase above into concrete actions
        driver.get(Utils.getUrl());

    }

    @When("I enter the login details for a {string}")
    public void i_enter_the_login_details_for_a(String userType) {

        switch (userType) {
            case "invalidUser":

                loginPage.enterUsername("abvcd");

                //invalid password
                loginPage.enterPassword("778787");

                loginPage.clickLoginButton();
                break;

            case "validUser":

                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();
                break;
            default:
                Assert.fail("The test data is wrong -the only values that can be accepted are 'validUser' or 'invalidUser'");

        }
    }

        @Then("I can see the following message: {string}")
        public void i_can_see_the_following_message (String validateMessage){
        switch (validateMessage){
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkFailedLoginWarning().contains(validateMessage));
                break;
            case "Login Successful":
                Assert.assertTrue(toolPage.successfullLoginMessage().contains(validateMessage));
                break;
                default:
                    Assert.fail("The test data is wrong");
        }
        }


    }
