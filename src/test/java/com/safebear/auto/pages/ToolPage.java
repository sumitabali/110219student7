package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.pages.locators.ToolPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolPage {

    //defining the webdriver
    @NonNull
    WebDriver driver;


    //link our toolpagelocator to this file
    ToolPageLocators locators = new ToolPageLocators();

    //check the successful login  message

    public String successfullLoginMessage() {
        return driver.findElement(locators.getSuccessfullLoginMessage()).getText();
    }
}





