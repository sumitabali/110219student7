package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    //link our loginpage locators tothis file
    LoginPageLocators locators=new LoginPageLocators();

    //Ensure we have a driver e.g chromedriver, firefoxdriver
    @NonNull
    WebDriver driver;

    public void enterUsername (String Username){

        driver.findElement(locators.getUsernameLocator()).sendKeys(Username);
    }

    public void enterPassword (String Password){

        driver.findElement(locators.getPasswordLocator()).sendKeys(Password);
    }


    public void clickLoginButton (){
        driver.findElement(locators.getLoginLocator()).click();
    }


    public String checkFailedLoginWarning (){
        return driver.findElement(locators.getFailedLoginMessage()).getText();
    }

}
