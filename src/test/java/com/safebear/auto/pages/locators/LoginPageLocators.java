package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {

    //fields
    private By usernameLocator =By.xpath(".//input[@id='username']");
    private By passwordLocator=By.xpath(".//input[@id='password']");

    //buttons
    private By loginLocator = By.xpath(".//button[@id='enter']");

    //message
    private By failedLoginMessage=By.xpath(".//b[.='WARNING: Username or Password is incorrect']");

}
