Feature: Test the weather search function on the BBC website
  As a user
  I want to know if I can able to search todays weather on the website

  Rules:
  *  Valid city/town should be entered
  *  Warning message displayed for incorrect city/town

  Scenario Outline:  Navigate to BBC website
    When I search for the valid city weather <city>
    Then the result should be displayed <ResultDisplayed>
    Examples:
    |city    |ResultDisplayed     |
    |Bristol |Result is displayed |
    |123     |Result not displayed|